import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:onemoney_sdk/ui/custom_button.dart';
import 'package:onemoney_sdk/ui/finatial_institution_screen.dart';
import 'package:onemoney_sdk/utils/app_sizes.dart';
import 'package:onemoney_sdk/utils/color_resources.dart';
import 'package:onemoney_sdk/utils/images.dart';
import 'package:onemoney_sdk/utils/size_utils/size_extension.dart';
import 'package:onemoney_sdk/utils/styles.dart';

class LinkAccountScreen extends StatefulWidget {
  const LinkAccountScreen({Key? key}) : super(key: key);

  @override
  _LinkAccountScreenState createState() => _LinkAccountScreenState();
}

class _LinkAccountScreenState extends State<LinkAccountScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        body: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 18.w),
            child: Column(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: 40.h,
                ),
                Text(
                  'Hi Rizwan,',
                  style: popinsBold.copyWith(fontSize: SizeConfig.TEXT_SIZE_HEADING),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 5.h),
                  child: Text(
                    "Let's take you through quick simple\nsteps to link your financial account.",
                    maxLines: 2,
                    style: popinsRegular.copyWith(
                        fontSize: SizeConfig.TEXT_SIZE_SUB_HEADING_, color: Colors.grey),
                  ),
                ),
                SizedBox(
                  height: 20.h,
                ),
                Center(
                  child: Image.asset(
                    Images.link_account,
                    width: 400.w,
                    height: 200.h,
                  ),
                ),
                SizedBox(
                  height: 5.h,
                ),
                Text(
                  "Link Accounts",
                  maxLines: 2,
                  style: popinsMedium.copyWith(
                      fontSize: SizeConfig.TEXT_SIZE_SUB_HEADING_LARGE,
                      color: Colors.black,
                      fontWeight: FontWeight.w600),
                ),
                SizedBox(
                  height: 5.h,
                ),
                Text(
                  "You can process your consent quickly by linking your Onemoney account to Bank,Investments, Insurances, Pensions to link",
                  style: popinsRegular.copyWith(
                    fontSize: 15.sp,
                    color: Colors.black87,
                  ),
                ),
                SizedBox(
                  height: 30.h,
                ),
                Text(
                  "We will need one of more of the following information for quickly linking your accounts",
                  textAlign: TextAlign.start,
                  style: popinsRegular.copyWith(
                    fontSize: 15.sp,
                    color: Colors.black87,
                  ),
                ),
                SizedBox(
                  height: 10.h,
                ),
                Row(
                  children: [
                    Text(
                      '• ',
                      style: popinsRegular.copyWith(color: Colors.black87, fontSize: 30.sp),
                    ),
                    Text('Mobile Number',
                        style: popinsRegular.copyWith(
                          fontSize: 15.sp,
                          color: Colors.black87,
                        )),
                  ],
                ),
                Row(
                  children: [
                    Text(
                      '• ',
                      style: popinsRegular.copyWith(color: Colors.black87, fontSize: 30.sp),
                    ),
                    Text('Customer Relationship Number',
                        style: popinsRegular.copyWith(
                          fontSize: 15.sp,
                          color: Colors.black87,
                        )),
                  ],
                ),
                Row(
                  children: [
                    Text(
                      '• ',
                      style: popinsRegular.copyWith(color: Colors.black87, fontSize: 30.sp),
                    ),
                    Text('Account Number',
                        style: popinsRegular.copyWith(
                          fontSize: 15.sp,
                          color: Colors.black87,
                        )),
                  ],
                ),
                Row(
                  children: [
                    Text(
                      '• ',
                      style: popinsRegular.copyWith(color: Colors.black87, fontSize: 30.sp),
                    ),
                    Text('Folio Number',
                        style: popinsRegular.copyWith(
                          fontSize: 15.sp,
                          color: Colors.black87,
                        )),
                  ],
                ),
                Row(
                  children: [
                    Text(
                      '• ',
                      style: popinsRegular.copyWith(color: Colors.black87, fontSize: 30.sp),
                    ),
                    Text('Policy Number',
                        style: popinsRegular.copyWith(
                          fontSize: 15.sp,
                          color: Colors.black87,
                        )),
                  ],
                ),
                SizedBox(height: 20.h,),
                Padding(
                  padding: EdgeInsets.only(bottom: 30.h),
                  child: Align(
                    child: CustomButton(
                      buttonText: "Add Account",
                      onTap: () {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (BuildContext context) => FinancialInstitutionScreen()));
                      },
                      buttonWidth: 190.w,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ));
  }
}
