import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:onemoney_sdk/ui/login_screen.dart';
import 'package:onemoney_sdk/ui/signup_screen.dart';
import 'package:onemoney_sdk/utils/images.dart';
import 'package:onemoney_sdk/utils/size_utils/screenutil_init.dart';
import 'package:onemoney_sdk/utils/size_utils/size_extension.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> with SingleTickerProviderStateMixin {
  AnimationController? _controller;
  Animation? _animation;

  @override
  void initState() {
    super.initState();

    _controller = AnimationController(vsync: this, duration: Duration(seconds: 1));

    _animation = Tween<double>(
      begin: 50,
      end: 500,
    ).animate(
      CurvedAnimation(
        parent: _controller!,
        curve: Curves.ease,
      ),
    );
    _animation!.addStatusListener((status) {
      if (AnimationStatus.completed == status) {
        // Navigator.of(context)
        //     .pushReplacement(MaterialPageRoute(builder: (BuildContext context) => SignUpScreen()));
        Navigator.of(context)
            .pushReplacement(MaterialPageRoute(builder: (BuildContext context) => LoginScreen()));
      }
    });
    _controller!.forward();
  }

  @override
  void dispose() {
    super.dispose();
    _controller!.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SizeUtilInit(
      designSize: Size(MediaQuery.of(context).size.width, MediaQuery.of(context).size.height),
      builder: () => Scaffold(
          body: Center(
        child: AnimatedBuilder(
          animation: _controller!,
          builder: (context, child) {
            return Image.asset(
              Images.one_money_logo,
              width: _animation!.value,
              height: 100.h,
            );
          },
        ),
      )),
    );
  }
}
