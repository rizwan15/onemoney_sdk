import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:onemoney_sdk/ui/custom_button.dart';
import 'package:onemoney_sdk/ui/link_account_screen.dart';
import 'package:onemoney_sdk/utils/app_sizes.dart';
import 'package:onemoney_sdk/utils/color_resources.dart';
import 'package:onemoney_sdk/utils/images.dart';
import 'package:onemoney_sdk/utils/size_utils/size_extension.dart';
import 'package:onemoney_sdk/utils/styles.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  @override
  Widget build(BuildContext context) {
    final node = FocusScopeNode();
    return Scaffold(
        resizeToAvoidBottomInset: false,
        body: Column(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 50.h,
            ),
            Image.asset(
              Images.one_money_logo,
              width: 180.w,
              height: 90.h,
            ),
            SizedBox(
              height: 20.h,
            ),
            Padding(
              padding: EdgeInsets.only(top: 5.h, left: 18.w),
              child: Text(
                'Welcome back!',
                style: popinsBold.copyWith(
                  fontSize: SizeConfig.TEXT_SIZE_HEADING,
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 5.h, left: 18.w),
              child: Text(
                'We are happy to see you need to signup\nfor manage your finance.',
                maxLines: 2,
                style: popinsRegular.copyWith(
                    fontSize: SizeConfig.TEXT_SIZE_SUB_HEADING_, color: Colors.grey),
              ),
            ),
            SizedBox(
              height: 20.h,
            ),
            Padding(
              padding: EdgeInsets.only(top: 5.h, left: 18.w),
              child: RichText(
                text: TextSpan(
                  text: 'If you are new / ',
                  style: popinsRegular.copyWith(
                      color: Colors.grey, fontSize: SizeConfig.TEXT_SIZE_SMALL),
                  children: const <TextSpan>[
                    TextSpan(
                        text: 'Create Account',
                        style: TextStyle(
                            fontWeight: FontWeight.bold, color: ColorResources.COLOR_PRIMARY)),
                  ],
                ),
              ),
            ),

            Padding(
              padding: EdgeInsets.only(top: 50.h, left: 18.w),
              child: Text('Mobile Number',
                  style: popinsRegular.copyWith(
                      color: Colors.grey, fontSize: SizeConfig.TEXT_SIZE_MEDIUM)),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 18.w, vertical: 10.h),
              child: TextField(
                autocorrect: true,
                decoration: InputDecoration(
                  filled: true,
                  fillColor: ColorResources.TEXT_FIELD_BACKGROUND,
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(12.0)),
                    borderSide: BorderSide(color: Colors.transparent, width: 2),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    borderSide: BorderSide(color: ColorResources.COLOR_PRIMARY),
                  ),
                ),
              ),
            ),

            Padding(
              padding: EdgeInsets.only(top: 20.h, left: 18.w),
              child: Text('PIN',
                  style: popinsRegular.copyWith(
                      color: Colors.grey, fontSize: SizeConfig.TEXT_SIZE_MEDIUM)),
            ),

            Padding(
              padding: EdgeInsets.symmetric(horizontal: 18.w, vertical: 10.h),
              child: PinCodeTextField(
                appContext: context,
                length: 6,
                obscureText: true,
                obscuringCharacter: '*',
                blinkWhenObscuring: true,
                animationType: AnimationType.fade,
                pinTheme: PinTheme(
                    shape: PinCodeFieldShape.box,
                    borderWidth: 0.9,
                    activeColor: ColorResources.COLOR_PRIMARY,
                    inactiveFillColor: ColorResources.TEXT_FIELD_BACKGROUND,
                    inactiveColor: ColorResources.TEXT_FIELD_BACKGROUND,
                    borderRadius: BorderRadius.circular(5),
                    fieldWidth: 50.w,
                    fieldHeight: 55.h,
                    activeFillColor: ColorResources.TEXT_FIELD_BACKGROUND,
                    selectedFillColor: ColorResources.TEXT_FIELD_BACKGROUND,
                    selectedColor: ColorResources.COLOR_PRIMARY),
                cursorColor: Colors.grey,
                animationDuration: Duration(milliseconds: 300),
                enableActiveFill: true,
                //  errorAnimationController: errorController,
                // controller: textEditingController,
                keyboardType: TextInputType.number,
                onCompleted: (v) {
                  print("Completed");
                },
                onChanged: (value) {
                  print(value);
                  setState(() {
                    //  currentText = value;
                  });
                },
              ),
            ),

            Center(
              child: TextButton(
                onPressed: () {},
                child: Text(
                  'Forgot PIN?',
                  textAlign: TextAlign.center,
                  style: popinsMedium.copyWith(
                      fontSize: SizeConfig.TEXT_SIZE_SMALL, color: ColorResources.COLOR_PRIMARY),
                ),
              ),
            ),
            Expanded(child: SizedBox()),
            //login button
            Padding(
              padding: EdgeInsets.only(bottom: 50.h),
              child: Align(
                child: CustomButton(
                  buttonText: "Sign In",
                  onTap: () {
                    Navigator.of(context).push(
                        MaterialPageRoute(builder: (BuildContext context) => LinkAccountScreen()));
                  },
                  buttonWidth: 150.w,
                ),
              ),
            ),
          ],
        ));
  }
}
