import 'package:flutter/material.dart';
import 'package:onemoney_sdk/utils/app_sizes.dart';
import 'package:onemoney_sdk/utils/color_resources.dart';
import 'package:onemoney_sdk/utils/size_utils/size_extension.dart';
import 'package:onemoney_sdk/utils/styles.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

class AccountLinkingBottomSheet extends StatelessWidget {
  const AccountLinkingBottomSheet({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
      height: 360.h,
      color: Colors.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Account Linking',
            style: popinsMedium.copyWith(color: Colors.black, fontSize: 19.sp),
          ),
          SizedBox(
            height: 10.h,
          ),
          Text(
            'For Axis Bank Account',
            style: popinsRegular.copyWith(color: Colors.black54, fontSize: 16.sp),
          ),
          SizedBox(
            height: 50.h,
          ),
          Text('OTP',
              style: popinsRegular.copyWith(
                  color: Colors.grey, fontSize: SizeConfig.TEXT_SIZE_MEDIUM)),
          SizedBox(
            height: 10.h,
          ),
          PinCodeTextField(
            appContext: context,
            length: 6,
            obscureText: true,
            obscuringCharacter: '*',
            blinkWhenObscuring: true,
            animationType: AnimationType.fade,
            pinTheme: PinTheme(
                shape: PinCodeFieldShape.box,
                borderWidth: 0.9,
                activeColor: ColorResources.COLOR_PRIMARY,
                inactiveFillColor: ColorResources.TEXT_FIELD_BACKGROUND,
                inactiveColor: ColorResources.TEXT_FIELD_BACKGROUND,
                borderRadius: BorderRadius.circular(5),
                fieldWidth: 50.w,
                fieldHeight: 55.h,
                activeFillColor: ColorResources.TEXT_FIELD_BACKGROUND,
                selectedFillColor: ColorResources.TEXT_FIELD_BACKGROUND,
                selectedColor: ColorResources.COLOR_PRIMARY),
            cursorColor: Colors.grey,
            animationDuration: Duration(milliseconds: 300),
            enableActiveFill: true,
            //  errorAnimationController: errorController,
            // controller: textEditingController,
            keyboardType: TextInputType.number,
            onCompleted: (v) {
              Navigator.pop(context);
            },
            onChanged: (value) {
              print(value);
              // setState(() {
              //   //  currentText = value;
              // });
            },
          ),
          SizedBox(
            height: 40.h,
          ),
          Align(
            alignment: Alignment.center,
            child: RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                text: "Didn't receive any code?\n",
                style: popinsRegular.copyWith(
                    color: Colors.grey, fontSize: 14.sp),
                children: const <TextSpan>[
                  TextSpan(
                      text: 'Re-Send Code',
                      style: TextStyle(
                          fontWeight: FontWeight.bold, color: ColorResources.COLOR_PRIMARY)),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
