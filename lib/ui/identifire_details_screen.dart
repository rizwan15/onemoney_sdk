import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:onemoney_sdk/model/color_model.dart';
import 'package:onemoney_sdk/ui/account_linking_screen.dart';
import 'package:onemoney_sdk/ui/custom_button.dart';
import 'package:onemoney_sdk/ui/identifier_details_item.dart';
import 'package:onemoney_sdk/utils/styles.dart';
import 'package:onemoney_sdk/utils/size_utils/size_extension.dart';

class IdentifireDetailsScreen extends StatefulWidget {
  const IdentifireDetailsScreen({Key? key}) : super(key: key);

  @override
  _IdentifireDetailsScreenState createState() => _IdentifireDetailsScreenState();
}

class _IdentifireDetailsScreenState extends State<IdentifireDetailsScreen> {
  int colorCount = -1;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          iconTheme: IconThemeData(
            color: Colors.black, //change your color here
          ),
          leading: IconButton(
            icon: Icon(Icons.arrow_back_ios, color: Colors.black),
            onPressed: () => Navigator.of(context).pop(),
          ),
          // backwardsCompatibility: true,
          backgroundColor: Colors.white,
          elevation: 0,
        ),
        body: Stack(
          fit: StackFit.expand,
          children: [
            SingleChildScrollView(
              physics: BouncingScrollPhysics(),
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 19.w),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: 15.h,
                    ),
                    Text(
                      'Provide Identifier Details',
                      style: popinsMedium.copyWith(color: Colors.black, fontSize: 20.sp),
                    ),
                    SizedBox(
                      height: 15.h,
                    ),
                    Text(
                      'Enter identifiers to enable discovery of your financial account',
                      style: popinsRegular.copyWith(color: Colors.black, fontSize: 16.sp),
                    ),
                    SizedBox(
                      height: 25.h,
                    ),
                    ListView.builder(
                        physics: NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                        itemCount: 2,
                        itemBuilder: (context, index) {
                          if (index > getColors().length - 1) {
                            colorCount++;
                            if (colorCount > getColors().length - 1) {
                              colorCount = -1;
                              colorCount++;
                            }
                          }

                          return IndentifierDetailsItem(
                            colorModel:
                                getColors()[index > getColors().length - 1 ? colorCount : index],
                          );
                        }),
                  ],
                ),
              ),
            ),
            Positioned(
              bottom: 1,
              left: 130.w,
              right: 130.w,
              child: Padding(
                padding: EdgeInsets.only(bottom: 20.h),
                child: CustomButton(
                  buttonText: "Continue",
                  onTap: () {
                    debugPrint("hello from button");
                    Navigator.of(context).push(
                        MaterialPageRoute(builder: (BuildContext context) => AccountLinkingScreen()));
                  },
                  buttonWidth: 100.w,
                ),
              ),
            ),
          ],
        ));
  }

  List<ColorModel> getColors() {
    List<ColorModel> list = [];

    list.add(ColorModel(color1: Color(0xFFA7BEF6), color2: Color(0xCC405EC9)));
    list.add(ColorModel(color1: Color(0xFFC5A7F6), color2: Color(0xCCBB207D)));
    list.add(ColorModel(color1: Color(0xFFE59482), color2: Color(0xCCF09968)));
    list.add(ColorModel(color1: Color(0xFF23FF95), color2: Color(0xCC68F0F0)));

    return list;
  }
}
