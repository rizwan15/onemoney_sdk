import 'package:flutter/material.dart';
import 'package:onemoney_sdk/ui/account_linking_bottom_sheet.dart';
import 'package:onemoney_sdk/utils/images.dart';
import 'package:onemoney_sdk/utils/size_utils/size_extension.dart';
import 'package:onemoney_sdk/utils/styles.dart';

class AccountItem extends StatefulWidget {
  @override
  _AccountItemState createState() => _AccountItemState();
}

class _AccountItemState extends State<AccountItem> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        showModalBottomSheet(
            context: context,
            isScrollControlled: true,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.vertical(
                top: Radius.circular(20),
              ),
            ),
            clipBehavior: Clip.antiAliasWithSaveLayer,
            builder: (context) {
              return Padding(
                padding: MediaQuery.of(context).viewInsets,
                child: AccountLinkingBottomSheet(),
              );
            });
      },
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 10.h),
        padding: EdgeInsets.symmetric(horizontal: 17.w, vertical: 18.h),
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(borderRadius: BorderRadius.circular(16.w), color: Colors.white),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Image.asset(
                  Images.saving_account,
                  width: 23.w,
                  height: 23.h,
                ),
                SizedBox(
                  width: 10,
                ),
                Text(
                  'Savings Account',
                  style: popinsMedium.copyWith(color: Colors.black87, fontSize: 15.sp),
                )
              ],
            ),
            SizedBox(
              height: 12,
            ),
            Text(
              'Account Number',
              style: popinsRegular.copyWith(color: Colors.grey, fontSize: 12.sp),
            ),
            SizedBox(
              height: 2,
            ),
            Text(
              'XXXX XXXX XXXX 1450',
              style: popinsMedium.copyWith(color: Colors.black87, fontSize: 14.sp),
            )
          ],
        ),
      ),
    );
  }
}
