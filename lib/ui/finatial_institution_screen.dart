import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:onemoney_sdk/model/color_model.dart';
import 'package:onemoney_sdk/ui/custom_button.dart';
import 'package:onemoney_sdk/ui/fianatial_instituation_item.dart';
import 'package:onemoney_sdk/ui/identifire_details_screen.dart';
import 'package:onemoney_sdk/utils/color_resources.dart';
import 'package:onemoney_sdk/utils/size_utils/size_extension.dart';
import 'package:onemoney_sdk/utils/styles.dart';

class FinancialInstitutionScreen extends StatefulWidget {
  const FinancialInstitutionScreen({Key? key}) : super(key: key);

  @override
  _FinancialInstitutionScreenState createState() => _FinancialInstitutionScreenState();
}

class _FinancialInstitutionScreenState extends State<FinancialInstitutionScreen> {
  int colorCount = -1;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          iconTheme: IconThemeData(
            color: Colors.black, //change your color here
          ),
          leading: IconButton(
            icon: Icon(Icons.arrow_back_ios, color: Colors.black),
            onPressed: () => Navigator.of(context).pop(),
          ),
          // backwardsCompatibility: true,
          backgroundColor: Colors.white,
          elevation: 0,
        ),
        body: Stack(
          fit: StackFit.expand,
          children: [
            SingleChildScrollView(
              physics: BouncingScrollPhysics(),
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 19.w),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: 15.h,
                    ),
                    Text(
                      'Financial Institution',
                      style: popinsMedium.copyWith(color: Colors.black, fontSize: 20.sp),
                    ),
                    SizedBox(
                      height: 15.h,
                    ),
                    Text(
                      'Select financial institution that you have accounts',
                      style: popinsRegular.copyWith(color: Colors.black, fontSize: 16.sp),
                    ),
                    SizedBox(
                      height: 30.h,
                    ),
                    TextField(
                      autocorrect: true,
                      decoration: InputDecoration(
                        hintText: 'Search Institution',
                        filled: true,
                        prefixIcon: Icon(Icons.search),
                        fillColor: ColorResources.TEXT_FIELD_BACKGROUND,
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(12.0)),
                          borderSide: BorderSide(color: Colors.transparent, width: 2),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(10.0)),
                          borderSide: BorderSide(color: ColorResources.COLOR_PRIMARY),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 10.h,
                    ),
                    ListView.builder(
                        physics: NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                        itemCount: 15,
                        itemBuilder: (context, index) {
                          if (index > getColors().length - 1) {
                            colorCount++;
                            if (colorCount > getColors().length - 1) {
                              colorCount = -1;
                              colorCount++;
                            }
                          }

                          return FinancialInstitutionItem(
                            colorModel:
                                getColors()[index > getColors().length - 1 ? colorCount : index],
                          );
                        }),
                  ],
                ),
              ),
            ),
            Positioned(
              bottom: 1,
              left: 130.w,
              right: 130.w,
              child: Padding(
                padding: EdgeInsets.only(bottom: 20.h),
                child: CustomButton(
                  buttonText: "Continue",
                  onTap: () {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (BuildContext context) => IdentifireDetailsScreen()));
                  },
                  buttonWidth: 100.w,
                ),
              ),
            ),
          ],
        ));
  }

  List<ColorModel> getColors() {
    List<ColorModel> list = [];

    list.add(ColorModel(color1: Color(0xFFA7BEF6), color2: Color(0xCC405EC9)));
    list.add(ColorModel(color1: Color(0xFFC5A7F6), color2: Color(0xCCBB207D)));
    list.add(ColorModel(color1: Color(0xFFE59482), color2: Color(0xCCF09968)));
    list.add(ColorModel(color1: Color(0xFF23FF95), color2: Color(0xCC68F0F0)));

    return list;
  }
}
