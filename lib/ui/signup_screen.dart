import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:onemoney_sdk/ui/custom_button.dart';
import 'package:onemoney_sdk/ui/verify_mobile.dart';
import 'package:onemoney_sdk/utils/app_sizes.dart';
import 'package:onemoney_sdk/utils/color_resources.dart';
import 'package:onemoney_sdk/utils/images.dart';
import 'package:onemoney_sdk/utils/size_utils/size_extension.dart';
import 'package:onemoney_sdk/utils/styles.dart';

class SignUpScreen extends StatefulWidget {
  const SignUpScreen({Key? key}) : super(key: key);

  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        body: Column(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 50.h,
            ),
            Image.asset(
              Images.one_money_logo,
              width: 180.w,
              height: 90.h,
            ),
            SizedBox(
              height: 20.h,
            ),
            Padding(
              padding: EdgeInsets.only(top: 5.h, left: 18.w),
              child: Text(
                'Welcome!',
                style: popinsBold.copyWith(fontSize: SizeConfig.TEXT_SIZE_HEADING),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 5.h, left: 18.w),
              child: Text(
                'We are happy to see you need to signup\nfor manage your finance.',
                maxLines: 2,
                style: popinsRegular.copyWith(
                    fontSize: SizeConfig.TEXT_SIZE_SUB_HEADING_, color: Colors.grey),
              ),
            ),
            SizedBox(
              height: 20.h,
            ),
            Padding(
              padding: EdgeInsets.only(top: 5.h, left: 18.w),
              child: RichText(
                text: TextSpan(
                  text: 'Already have an account / ',
                  style: popinsRegular.copyWith(
                      color: Colors.grey, fontSize: SizeConfig.TEXT_SIZE_SMALL),
                  children: const <TextSpan>[
                    TextSpan(
                        text: 'Sign In',
                        style: TextStyle(
                            fontWeight: FontWeight.bold, color: ColorResources.COLOR_PRIMARY)),
                  ],
                ),
              ),
            ),

            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: EdgeInsets.only(top: 50.h, left: 18.w),
                  child: Text('Name',
                      style: popinsRegular.copyWith(
                          color: Colors.grey, fontSize: SizeConfig.TEXT_SIZE_MEDIUM)),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 18.w, vertical: 10.h),
                  child: TextField(
                    autocorrect: true,
                    decoration: InputDecoration(
                      filled: true,
                      fillColor: ColorResources.TEXT_FIELD_BACKGROUND,
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(12.0)),
                        borderSide: BorderSide(color: Colors.transparent, width: 2),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                        borderSide: BorderSide(color: ColorResources.COLOR_PRIMARY),
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 20.h, left: 18.w),
                  child: Text('Mobile Number',
                      style: popinsRegular.copyWith(
                          color: Colors.grey, fontSize: SizeConfig.TEXT_SIZE_MEDIUM)),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 18.w, vertical: 10.h),
                  child: TextField(
                    autocorrect: true,
                    keyboardType: TextInputType.phone,
                    decoration: InputDecoration(
                      filled: true,
                      fillColor: ColorResources.TEXT_FIELD_BACKGROUND,
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(12.0)),
                        borderSide: BorderSide(color: Colors.transparent, width: 2),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                        borderSide: BorderSide(color: ColorResources.COLOR_PRIMARY),
                      ),
                    ),
                  ),
                ),
              ],
            ),

            Padding(
              padding: EdgeInsets.only(top: 30.h, left: 18.w),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  RichText(
                    text: TextSpan(
                      text: 'I agree to Onemoney ',
                      style: popinsRegular.copyWith(
                          color: Colors.grey, fontSize: SizeConfig.TEXT_SIZE_SMALL),
                      children: <TextSpan>[
                        TextSpan(
                            text: 'Terms and Conditions',
                            style: popinsMedium.copyWith(
                                fontSize: SizeConfig.TEXT_SIZE_SMALL,
                                color: ColorResources.COLOR_PRIMARY)),
                      ],
                    ),
                  )
                ],
              ),
            ),
            Expanded(child: SizedBox()),
            //login button
            Padding(
              padding: EdgeInsets.only(bottom: 50.h),
              child: Align(
                child: CustomButton(
                  buttonText: "Let's Get Started",
                  onTap: () {
                    Navigator.of(context).push(
                        MaterialPageRoute(builder: (BuildContext context) => VerifyMobileScreen()));
                  },
                  buttonWidth: 200.w,
                ),
              ),
            ),
          ],
        ));
  }
}
