import 'package:flutter/material.dart';
import 'package:onemoney_sdk/model/color_model.dart';
import 'package:onemoney_sdk/ui/link_account_main_item.dart';
import 'package:onemoney_sdk/utils/size_utils/size_extension.dart';
import 'package:onemoney_sdk/utils/styles.dart';

class AccountLinkingScreen extends StatefulWidget {
  const AccountLinkingScreen({Key? key}) : super(key: key);

  @override
  _AccountLinkingScreenState createState() => _AccountLinkingScreenState();
}

class _AccountLinkingScreenState extends State<AccountLinkingScreen> {
  int colorCount = -1;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          iconTheme: IconThemeData(
            color: Colors.black, //change your color here
          ),
          leading: IconButton(
            icon: Icon(Icons.arrow_back_ios, color: Colors.black),
            onPressed: () => Navigator.of(context).pop(),
          ),
          // backwardsCompatibility: true,
          backgroundColor: Colors.white,
          elevation: 0,
        ),
        body: SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 19.w),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: 15.h,
                ),
                Text(
                  'Account Linking',
                  style: popinsMedium.copyWith(color: Colors.black, fontSize: 20.sp),
                ),
                SizedBox(
                  height: 15.h,
                ),
                Text(
                  'You are almost done,please choose the accounts you would like to link',
                  style: popinsRegular.copyWith(color: Colors.black, fontSize: 16.sp),
                ),
                SizedBox(
                  height: 25.h,
                ),
                ListView.builder(
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: 2,
                    itemBuilder: (context, index) {
                      if (index > getColors().length - 1) {
                        colorCount++;
                        if (colorCount > getColors().length - 1) {
                          colorCount = -1;
                          colorCount++;
                        }
                      }

                      return LinkAccountMainItem(
                        colorModel:
                            getColors()[index > getColors().length - 1 ? colorCount : index],
                      );
                    }),
              ],
            ),
          ),
        ));
  }

  List<ColorModel> getColors() {
    List<ColorModel> list = [];

    list.add(ColorModel(color1: Color(0xFFA7BEF6), color2: Color(0xCC405EC9)));
    list.add(ColorModel(color1: Color(0xFFC5A7F6), color2: Color(0xCCBB207D)));
    list.add(ColorModel(color1: Color(0xFFE59482), color2: Color(0xCCF09968)));
    list.add(ColorModel(color1: Color(0xFF23FF95), color2: Color(0xCC68F0F0)));

    return list;
  }
}
